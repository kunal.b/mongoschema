var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "name": "Oneplus 8",
        "thumbnail": "some url",
        "product_gallery": [{
            "front_view": "front url"
        },
        {
            "side_view": "side url"
        },
        {
            "back_view" : "back url"
        }],
        "description" : "Some description",
        "base_price" : 40000,
        "sell_price" : 35000
    },
    {
        "name": "4k TV",
        "thumbnail": "some url",
        "product_gallery": [{
            "front_view": "front url"
        },
        {
            "side_view": "side url"
        },
        {
            "back_view" : "back url"
        }],
        "description" : "Some description",
        "base_price" : 20000,
        "sell_price" : 17000
    },
    {
        "name": "Bose Revolve Soundlink",
        "thumbnail": "some url",
        "product_gallery": [{
            "front_view": "front url"
        },
        {
            "side_view": "side url"
        },
        {
            "back_view" : "back url"
        }],
        "description" : "Some description",
        "base_price" : 20000,
        "sell_price" : 15000
    },
    {
        "name": "Charger",
        "thumbnail": "some url",
        "product_gallery": [{
            "front_view": "front url"
        },
        {
            "side_view": "side url"
        },
        {
            "back_view" : "back url"
        }],
        "description" : "Some description",
        "base_price" : 400,
        "sell_price" : 350
    },
    {
        "name": "Electric Guitar",
        "thumbnail": "some url",
        "product_gallery": [{
            "front_view": "front url"
        },
        {
            "side_view": "side url"
        },
        {
            "back_view" : "back url"
        }],
        "description" : "Some description",
        "base_price" : 10000,
        "sell_price" : 6000
    },
    {
        "name": "iphone 12",
        "thumbnail": "some url",
        "product_gallery": [{
            "front_view": "front url"
        },
        {
            "side_view": "side url"
        },
        {
            "back_view" : "back url"
        }],
        "description" : "Some description",
        "base_price" : 90000,
        "sell_price" : 85000
    }]

    const db = client.db("ecommerce");
    db.collection("Products").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + "records inserted")

    })


    const myquery = { name: "Electric Guitar" };
    db.collection("Products").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");


        client.close();
    });




})