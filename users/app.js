var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "first_name": "Kunal",
        "last_name": "Bisht",
        "email": "kbisht45@gmail.com",
        "profile_image": "some url",
        "role": "Software Engineer Trainee"
    },
    {
        "first_name": "Devansh",
        "last_name": "Dhawan",
        "email": "dhawan44@gmail.com",
        "profile_image": "some url",
        "role": "Software Engineer Trainee"
    },
    {
        "first_name": "Navdeep",
        "last_name": "Ruhil",
        "email": "navs@gmail.com",
        "profile_image": "some url",
        "role": "Software Engineer Trainee"
    },
    {
        "first_name": "Divy",
        "last_name": "Bhardwaj",
        "email": "bhardwaj@gmail.com",
        "profile_image": "some url",
        "role": "Software Engineer Trainee"
    },
    {
        "first_name": "Yuvraj",
        "last_name": "Singh",
        "email": "uvsingh@gmail.com",
        "profile_image": "some url",
        "role": "Software Engineer Trainee"
    },
    {
        "first_name": "Shaswat",
        "last_name": "Shukla",
        "email": "sshukla@gmail.com",
        "profile_image": "some url",
        "role": "Software Engineer Trainee"
    }]

    const db = client.db("ecommerce");
    db.collection("Users").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + "records inserted")

    })


    const myquery = { first_name: "Shaswat" };
    db.collection("Users").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");


        client.close();
    });




})