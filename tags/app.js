var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "name": "type1",
        "slug" : "Some slug"
    },
    {
        "name": "type2",
        "slug" : "Some slug"
    },
    {
        "name": "type3",
        "slug" : "Some slug"
    },
    {
        "name": "type4",
        "slug" : "Some slug"
    },
    {
        "name": "type5",
        "slug" : "Some slug"
    },
    {
        "name": "type6",
        "slug" : "Some slug"
    },]

    const db = client.db("ecommerce");
    db.collection("Types").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + "records inserted")
    })


    const myquery = { name : "type4" };
    db.collection("Types").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");


        client.close();
    });




})