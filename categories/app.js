var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "name": "Furniture",
        "slug": "abcxyz",
        "image": "some url",
        "description": "some description"
    },
    {
        "name": "Utensils",
        "slug": "abcxyz",
        "image": "some url",
        "description": "some description"
    },
    {
        "name": "Clothings",
        "slug": "abcxyz",
        "image": "some url",
        "description": "some description"
    },
    {
        "name": "Electronics",
        "slug": "abcxyz",
        "image": "some url",
        "description": "some description"
    },
    {
        "name": "Footwear",
        "slug": "abcxyz",
        "image": "some url",
        "description": "some description"
    },
    {
        "name": "Jewellery",
        "slug": "abcxyz",
        "image": "some url",
        "description": "some description"
    },
    ]

    const db = client.db("ecommerce");
    db.collection("Categories").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + "records inserted")

    })


    const myquery = { name: "Jewellery" };
    db.collection("Categories").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");


        client.close();
    });




})