var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "product" : "Chair",
        "user" : "Kunal",
        "product_quantity" : 10,
        "base_price" : 1500,
        "sell_price" : 2000,
        "total_price" : 20000
    },
    {
        "product" : "Bed",
        "user" : "Geeta",
        "product_quantity" : 10,
        "base_price" : 1500,
        "sell_price" : 2000,
        "total_price" : 20000
    },
    {
        "product" : "Sofa",
        "user" : "Lalit",
        "product_quantity" : 10,
        "base_price" : 1500,
        "sell_price" : 2000,
        "total_price" : 20000
    },
    {
        "product" : "Candles",
        "user" : "Geetika",
        "product_quantity" : 10,
        "base_price" : 150,
        "sell_price" : 200,
        "total_price" : 2000
    },
    {
        "product" : "Side Table",
        "user" : "Manas",
        "product_quantity" : 2,
        "base_price" : 1500,
        "sell_price" : 2000,
        "total_price" : 4000
    },
    {
        "product" : "Charger",
        "user" : "Devansh",
        "product_quantity" : 2,
        "base_price" : 150,
        "sell_price" : 200,
        "total_price" : 400
    },]


    const db = client.db("ecommerce");
    db.collection("Carts").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + " inserted")
    })


   const myquery = { user: "Devansh" };
    db.collection("Carts").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");


        client.close();
    });




})