var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "name" : "Intern",
        "slug" : "Some slug"
    },
    {
        "name" : "Software Engineer Trainee",
        "slug" : "Some slug"
    },
    {
        "name" : "Mover",
        "slug" : "Some slug"
    },
    {
        "name" : "Receptionist",
        "slug" : "Some slug"
    },
    {
        "name" : "Manager",
        "slug" : "Some slug"
    },
    {
        "name" : "Boss",
        "slug" : "Some slug"
    },]

    const db = client.db("ecommerce");
    db.collection("Roles").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + "records inserted")
    })


    const myquery = { name: "Boss" };
    db.collection("Roles").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");


        client.close();
    })




})