var { MongoClient } = require("mongodb");

var url = "mongodb://localhost:27017";

MongoClient.connect(url, function (err, client) {
    if (err)
        throw err;

    var obj = [{
        "users_id": 1,
        "total_item": 5,
        "products": [{
            "bed": 2
        },
        {
            "sofa": 1
        },
        {
            "charger": 2
        }],
        "billing_address": "Delhi",
        "shipping_address": "Uttarakhand",
        "transaction)status" : "Dispatched"
    },
    {
        "users_id": 2,
        "total_item": 5,
        "products": [{
            "bed": 2
        },
        {
            "sofa": 1
        },
        {
            "charger": 2
        }],
        "billing_address": "Delhi",
        "shipping_address": "Mumbai",
        "transaction)status" : "Dispatched"
    },
    {
        "users_id": 3,
        "total_item": 5,
        "products": [{
            "bed": 2
        },
        {
            "sofa": 1
        },
        {
            "charger": 2
        }],
        "billing_address": "Delhi",
        "shipping_address": "Dehradun",
        "transaction)status" : "Dispatched"
    },
    {
        "users_id": 4,
        "total_item": 5,
        "products": [{
            "bed": 2
        },
        {
            "sofa": 1
        },
        {
            "charger": 2
        }],
        "billing_address": "Delhi",
        "shipping_address": "Goa",
        "transaction)status" : "Dispatched"
    },
    {
        "users_id": 5,
        "total_item": 5,
        "products": [{
            "bed": 2
        },
        {
            "sofa": 1
        },
        {
            "charger": 2
        }],
        "billing_address": "Delhi",
        "shipping_address": "Manipur",
        "transaction)status" : "Dispatched"
    },
    {
        "users_id": 6,
        "total_item": 5,
        "products": [{
            "bed": 2
        },
        {
            "sofa": 1
        },
        {
            "charger": 2
        }],
        "billing_address": "Delhi",
        "shipping_address": "Srinagar",
        "transaction)status" : "Dispatched"
    },]

    const db = client.db("ecommerce");
    db.collection("Orders").insertMany(obj, function (err, res) {
        if (err)
            throw err;
        console.log(res.insertedCount + "records inserted")

    })


    const myquery = { shipping_address: "Srinagar" };
    db.collection("Orders").deleteOne(myquery, function (err, res) {
        if (err)
            throw err;
        console.log("record(s) deleted");
    
    
        client.close();
    });




})